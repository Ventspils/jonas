'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

/////////////////////////////////////////////////
// Data

// DIFFERENT DATA! Contains movement dates, currency and locale

const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
  interestRate: 1.2, // %
  pin: 1111,

  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2021-01-04T17:01:17.194Z',
    '2021-01-06T21:36:17.929Z',
    '2021-01-08T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'lv-LV', // de-DE
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,

  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
};

const accounts = [account1, account2];

/////////////////////////////////////////////////
// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

/////////////////////////////////////////////////
// Functions
const formatMovementDate = function (date, locale) {
  const calcDaysBetween = (date1, date2) =>
    Math.round(Math.abs(date2 - date1) / (1000 * 60 * 60 * 24));
  const daysPassed = calcDaysBetween(new Date(), date);

  if (daysPassed === 0) return 'Today';
  if (daysPassed === 1) return 'Yesterday';
  if (daysPassed <= 7) return `${daysPassed} days ago`;
  else {
    // const day = `${date.getDate()}`.padStart(2, 0);
    // const month = `${date.getMonth() + 1}`.padStart(2, 0);
    // const year = date.getFullYear();
    // return `${day}.${month}.${year}`;
    return new Intl.DateTimeFormat(locale).format(date);
  }
};

const formatCur = function (value, locale, currency) {
  return Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency,
  }).format(value);
};

const printMovements = function (account, sorted = false) {
  containerMovements.innerHTML = '';

  const movs = sorted
    ? account.movements.slice().sort((a, b) => a - b)
    : account.movements;

  movs.forEach((mov, i) => {
    const type = mov > 0 ? 'deposit' : 'withdrawal';
    const movDate = new Date(account.movementsDates[i]);

    const formatedMov = formatCur(mov, account.locale, account.currency);

    const html = `
      <div class="movements__row">
        <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
    <div class="movements__date">${formatMovementDate(
      movDate,
      account.locale
    )}</div>
        <div class="movements__value">${formatedMov}</div>
      </div>
    `;
    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

const displayBalance = function (account) {
  account.balance = account.movements.reduce((prev, curr) => prev + curr);
  labelBalance.textContent = formatCur(
    account.balance,
    account.locale,
    account.currency
  );
};

const displaySummary = function (account) {
  const sumIn = account.movements
    .filter((mov) => mov > 0)
    .reduce((acc, mov) => acc + mov);
  labelSumIn.textContent = formatCur(sumIn, account.locale, account.currency);

  const sumOut = Math.abs(
    account.movements.filter((mov) => mov < 0).reduce((acc, mov) => acc + mov)
  );
  labelSumOut.textContent = formatCur(sumOut, account.locale, account.currency);

  const intSum = account.movements
    .filter((mov) => mov > 0)
    .map((deposit) => (deposit * account.interestRate) / 100)
    .filter((int) => int >= 1) // only add interest > 1.00 eur
    .reduce((acc, int) => acc + int);
  labelSumInterest.textContent = formatCur(
    intSum,
    account.locale,
    account.currency
  );
};

const createUsernames = (accs) => {
  accs.forEach((acc) => {
    acc.username = acc.owner
      .toLowerCase()
      .trim()
      .split(' ')
      .map((usr) => usr[0])
      .join('');
  });
};
createUsernames(accounts);

const updateUI = function (account) {
  // Display movements
  printMovements(account);
  // Display balance
  displayBalance(account);
  // Display summary
  displaySummary(account);
  // Reset timer
  clearInterval(timer);
  timer = logoutTimer();
};

const logoutTimer = function () {
  // Set time to 5 minutes
  let time = 120;
  const tick = function () {
    const min = String(Math.trunc(time / 60)).padStart(2, 0);
    const sec = String(time % 60).padStart(2, 0);
    labelTimer.textContent = `${min}:${sec}`;
    if (time === 0) {
      clearInterval(timer);
      labelWelcome.textContent = 'Log back in to get started!';
      containerApp.style.opacity = 0;
    }
    time--;
  };
  tick();
  // Call the timer every second
  const timer = setInterval(tick, 1000);
  return timer;
};

// SET INITIAL VALUES
let currentAccount, timer;
let sorted = false;

// FAKE LOGGING IN
// currentAccount = account1;
// updateUI(currentAccount);
// containerApp.style.opacity = 100;
// logoutTimer();

// Experimenting with Internationalixation API
// const now = new Date();

// LOGIN
btnLogin.addEventListener('click', function (e) {
  e.preventDefault();
  currentAccount = accounts.find(
    (acc) => acc.username === inputLoginUsername.value
  );

  if (currentAccount?.pin === +inputLoginPin.value) {
    //Clear login fields
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();
    //Display UI and message
    containerApp.style.opacity = 100;
    labelWelcome.textContent = `Welcome back, ${
      currentAccount.owner.split(' ')[0]
    }`;

    const now = new Date();
    const options = {
      hour: 'numeric',
      minute: 'numeric',
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      // weekday: 'short',
    };
    const locale = currentAccount.locale;
    labelDate.textContent = Intl.DateTimeFormat(locale, options).format(now);
    // Start timer
    if (timer) clearInterval(timer);
    timer = logoutTimer();
    updateUI(currentAccount);
  }
});

// TRANSFER FUNDS
btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = +inputTransferAmount.value;
  const transferAccTo = accounts.find(
    (acc) => acc.username === inputTransferTo.value
  );
  //Clear transfer fields
  inputTransferAmount.value = inputTransferTo.value = '';

  if (
    transferAccTo &&
    transferAccTo !== currentAccount &&
    amount > 0 &&
    currentAccount.balance >= amount
  ) {
    // Updating account records
    currentAccount.movements.push(-amount);
    transferAccTo.movements.push(amount);
    currentAccount.movementsDates.push(new Date().toISOString());
    transferAccTo.movementsDates.push(Date.now());
    updateUI(currentAccount);
  }
});

// REQUEST A LOAN
btnLoan.addEventListener('click', function (e) {
  e.preventDefault();

  const loanAmount = Math.floor(inputLoanAmount.value);
  if (
    // loanAmount &&
    loanAmount > 0 &&
    currentAccount.movements.some((mov) => mov * 0.1 >= loanAmount)
  ) {
    const loanTimer = setTimeout(() => {
      console.log('Loan granted');
      currentAccount.movements.push(loanAmount);
      currentAccount.movementsDates.push(new Date().toISOString());
      updateUI(currentAccount);
    }, 3000);
  } else {
    console.log('Loan not granted');
  }
});

// CLOSE ACCOUNT
btnClose.addEventListener('click', function (e) {
  e.preventDefault();
  if (
    currentAccount.username === inputCloseUsername.value &&
    currentAccount.pin === +inputClosePin.value
  ) {
    const deleteIndex = accounts.findIndex(
      (acc) => acc.username === currentAccount.username
    );
    accounts.splice(deleteIndex, 1);
    containerApp.style.opacity = 0;
    inputCloseUsername.value = inputClosePin.value = '';
  }
});

// SORT MOVEMENTS
btnSort.addEventListener('click', function (e) {
  e.preventDefault();
  printMovements(currentAccount.movements, !sorted);
  sorted = !sorted;
  console.log('Sorted!');
});

// DISPLAY DATES

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES
console.log(Number.parseInt('30px', 10));
console.log(Number.parseFloat('2.5rem'));

console.log(Number.isNaN(+'2x'));

console.log(Number.isFinite(+'33'));

console.log(1528 % 2);

const isEven = (n) => n % 2 === 0;

console.log(isEven(0));

labelBalance.addEventListener('click', () => {
  [...document.querySelectorAll('.movements__row')].forEach((row, i) => {
    if (i % 2 === 0) row.style.backgroundColor = '#f4f4b9';
  });
});

console.log(2 ** 53 + 1);
console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MIN_SAFE_INTEGER);

console.log(959595236654943219819132194949n);

// Dates
console.log(new Date());

const future = new Date(2037, 10, 19, 15, 23);
console.log(+future);

// const calcDaysBetween1 = (date1, date2) =>
//   Math.abs(date2 - date1) / (1000 * 60 * 60 * 24);

// console.log(calcDaysBetween1(new Date(2021, 3, 27), Date.now()))

const num = 435913454819.23;
const options = {
  style: 'currency',
  currency: 'EUR',
};

console.log('US:', Intl.NumberFormat('en-US', options).format(num));
console.log('RUS:', Intl.NumberFormat('ru-RU', options).format(num));
console.log('GB:', Intl.NumberFormat('en-GB', options).format(num));
console.log('LV:', Intl.NumberFormat('lv-LV', options).format(num));

const ingredients = ['olives', ''];
const pizzaTimer = setTimeout(
  (ing1, ing2) => console.log(`Here is your pizza with ${ing1} and ${ing2}`),
  3000,
  ...ingredients
);
console.log('Waiting...');

if (ingredients.includes('spinach')) clearTimeout(pizzaTimer);

// setInterval(function () {
//   const now = new Date();
//   const hour = now.getHours().toString().padStart(2, 0);
//   const minute = now.getMinutes().toString().padStart(2, 0);
//   const sec = now.getSeconds().toString().padStart(2, 0);
//   console.log(`${hour}:${minute}:${sec}`);
// }, 1000);
