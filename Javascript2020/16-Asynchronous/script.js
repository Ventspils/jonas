'use strict';

const btn = document.querySelector('.btn-country');
const countriesContainer = document.querySelector('.countries');

///////////////////////////////////////
const renderCountry = function (data, neighbourClass = '') {
  const html = `
  <article class="country ${neighbourClass}">
  <img class="country__img" src="${data.flag}" />
  <div class="country__data">
  <h3 class="country__name">${data.name}</h3>
  <h4 class="country__region">${data.region}</h4>
  <p class="country__row"><span>👫</span>${(data.population / 1000000).toFixed(
    1
  )}M people</p>
  <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
  <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
  </div>
  </article>
  `;

  countriesContainer.insertAdjacentHTML('beforeend', html);
  countriesContainer.style.opacity = 1;
};

const renderError = function (msg) {
  countriesContainer.insertAdjacentText('beforebegin', msg);
  // countriesContainer.style.opacity = 1;
};
// XMLHTTPREQUEST
// const getCountryDataAndNeighbour = function (country) {
//   const request = new XMLHttpRequest();
//   request.open('GET', `https://restcountries.eu/rest/v2/name/${country}`);
//   request.send();
//   console.log(request.responseText);
//   request.addEventListener('load', () => {
//     const [data] = JSON.parse(request.responseText); // Destructuring array with object
//     renderCountry(data);
//   });
// };

// getCountryDataAndNeighbour('latvia');
// PROMISES

// const getCountryData = function (country) {
//   fetch(`https://restcountries.eu/rest/v2/name/${country}`)
//     .then((response) => {
//       console.log(response);
//       if (!response.ok)
//         throw new Error(`Country not found (${response.status})`);
//       return response.json();
//     })
//     .then((data) => {
//       renderCountry(data[0]);
//       const neigbour = data[0].borders[0];
//       console.log(neigbour);
//       if (!neigbour) return;
//       return fetch(`https://restcountries.eu/rest/v2/alpha/${neigbour}`);
//     })
//     .then((response) =>
//       response.json().then((data) => renderCountry(data, 'neighbour'))
//     )
//     .catch((err) => {
//       console.log(err);
//       renderError(`Something went wrong: ${err.message}!`);
//     })
//     .finally(() => {
//       countriesContainer.style.opacity = 1;
//     });
// };

const getJSON = function (url, errMsg = 'Something went wrong') {
  return fetch(url).then((response) => {
    if (!response.ok) throw new Error(`${errMsg}(${response.status})`);
    return response.json();
  });
};

const getCountryData = function (country) {
  getJSON(
    `https://restcountries.eu/rest/v2/name/${country}`,
    'Country not found'
  )
    .then((data) => {
      renderCountry(data[0]);
      const neigbour = data[0].borders[0];
      console.log(neigbour);
      if (!neigbour) throw new Error('No neighboring country exist');
      return getJSON(
        `https://restcountries.eu/rest/v2/alpha/${neigbour}`,
        'Neighbour country not found'
      );
    })
    .then((data) => renderCountry(data, 'neighbour'))
    .catch((err) => {
      console.log(err);
      renderError(`${err.message}!`);
    })
    .finally(() => {
      countriesContainer.style.opacity = 1;
    });
};

btn.addEventListener('click', function () {
  getCountryData('congo');
});
// EVENT LOOP TEST
console.log('Start test');
setTimeout(() => console.log('0 second timer'), 0);
Promise.resolve('Resolved promise 1').then((response) => console.log(response));
console.log('End test');
///

const lotteryPromise = new Promise(function (resolve, reject) {
  console.log('Lottery draw just started');
  setTimeout(function () {
    if (Math.random() > 0.5) {
      resolve('You WIN!');
    } else {
      reject(new Error('You lost!'));
    }
  }, 2000);
});

lotteryPromise
  .then((res) => console.log(res))
  .catch((err) => console.error(err));

const wait = function (seconds) {
  return new Promise(function (resolve) {
    setTimeout(resolve, seconds * 1000);
  });
};

// wait(2)
//   .then(() => {
//     console.log('waited 2 seconds');
//     return wait(3);
//   })
//   .then(() => {
//     console.log('waited 3 seconds');
//     return wait(1);
//   })
//   .then(() => {
//     console.log('waited 1 second');
//   });

// Promisefy Geolocation API
const getPosition = function () {
  // return new Promise(function (resolve, reject) {
  //   navigator.geolocation.getCurrentPosition(
  //     (pos) => resolve(pos),
  //     (err) => reject(err)
  //   );
  // });
  return new Promise(function (resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
};

// getPosition().then((pos) => console.log(pos));

// ASYNC AWAIT
const whereAmIAcync = async function () {
  // fetch(`https://restcountries.eu/rest/v2/name/${country}`).then((res) =>
  //   console.log(res)
  // );

  try {
    const location = await getPosition();
    const { latitude: lat, longitude: lon } = location.coords;
    console.log(lat, lon);
    const resGeo = await fetch(
      `https://us1.locationiq.com/v1/reverse.php?key=pk.85ba6bc16f21f87c70351cbae9bd3105	&lat=${lat}&lon=${lon}&format=json`
    );
    if (!resGeo.ok) throw new Error('Problem getting location');
    const dataGeo = await resGeo.json();
    const country = dataGeo.address.country;

    const res = await fetch(`https://restcountries.eu/rest/v2/name/${country}`);
    if (!res.ok) throw new Error('Problem getting country information');
    const data = await res.json();
    renderCountry(data[0]);

    return `You are in ${dataGeo.address.city}`;
  } catch (err) {
    console.log(`${err.message} 😕`);
  }
};
console.log('1: Will get location');
// whereAmIAcync().then((city) => console.log(city));

(async function () {
  try {
    const city = await whereAmIAcync();
    console.log(city);
  } catch (err) {
    console.log(err.message);
  }
  console.log('3: Finished getting location');
})();

const timeout = function (sec) {
  return new Promise(function (_, reject) {
    setTimeout(function () {
      reject(new Error('Request took too long'));
    }, sec * 1000);
  });
};

Promise.race([
  getJSON('https://restcountries.eu/rest/v2/name/usa'),
  timeout(0.5),
])
  .then((res) => console.log(res[0]))
  .catch((err) => console.log(err.message));
