'use strict';

const wait = function (seconds) {
  return new Promise(function (resolve) {
    setTimeout(resolve, seconds * 1000);
  });
};
// const divImages = document.querySelector('#container');
const divImages = document.querySelector('.images');

const createImage = function (imgPath) {
  return new Promise(function (resolve, reject) {
    const img = document.createElement('img');
    img.setAttribute('src', imgPath);
    img.addEventListener('load', function () {
      divImages.appendChild(img);
      resolve(img);
    });
    img.addEventListener('error', () =>
      reject(new Error('Картинка не найдена!'))
    );
  });
};

let currentImage;

// createImage('./img/img-1.jpg')
//   .then((img) => {
//     currentImage = img;
//     console.log('Image 1 loaded');
//     return wait(2);
//   })
//   .then(() => {
//     currentImage.style.display = 'none';
//     return createImage('./img/img-2.jpg');
//   })
//   .then((img) => {
//     currentImage = img;
//     console.log('Image 2 loaded');
//     return wait(2);
//   })
//   .then(() => {
//     currentImage.style.display = 'none';
//   })

//   .catch((err) => console.log('Image not found and not loaded!'));

const loadNPause = async function () {
  try {
    currentImage = await createImage('img/img-1.jpg');
    console.log('Image 1 loaded');
    await wait(5);
    currentImage.style.display = 'none';
    await wait(5);
    currentImage = await createImage('img/img-2.jpg');
    console.log('Image 2 loaded');
    await wait(5);
    console.log('Waited 5 seconds');
    currentImage.style.display = 'none';
  } catch (err) {
    console.log(err.message);
  }
};

// loadNPause();

const loadAll = async function (imgArr) {
  try {
    const imgs = await imgArr.map((path) => createImage(path));
    console.log(imgs);
    const images = await Promise.all(imgs);
    images.forEach((img) => img.classList.add('parallel'));
  } catch (err) {
    console.error(err.message);
  }
};

loadAll(['img/img-1.jpg', 'img/img-2.jpg', 'img/img-3.jpg']);
