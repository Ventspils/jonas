'use strict';

console.log('------ SCRIPT.JS ------');

// let hasDriversLicense = false;
// const passTest = true;

// if (passTest) hasDriversLicense = true;
// if (hasDriversLicense) console.log('I can drive');

// const interface = 'Audio';
// const private = 333;

// function logger(msg) {
//   console.log(msg);
// }

// logger('Hello!');
// logger('world!');

// const calcAge = function (birthYear) {
//   return 2089 - birthYear;
// };
// console.log(calcAge(1975));

// Arrow functions
// const calcAge3 = (birthYear) => 2037 - birthYear;
// console.log(calcAge3(1991));

// const yearsUntilRetirement = (birthYear, firstName) => {
//   const age = 2037 - birthYear;
//   return `${firstName} retires in ${65 - age} years.`;
// };
// console.log(yearsUntilRetirement(1991, 'Andruxa'));

// const piecesCutter = function (fruit) {
//   return fruit * 3;
// };

// const fruitProcessor = function (apples, oranges) {
//   const applePieces = piecesCutter(apples);
//   const orangePieces = piecesCutter(oranges);
//   const juice = `Juice with ${applePieces} apple pieces and ${orangePieces} orange pieces.`;
//   return juice;
// };

// console.log(fruitProcessor(2, 3));

// const calcAge = function (birthYear) {
//   return 2020 - birthYear;
// };

// const yearsUntilRetirement = function (birthYear, firstName) {
//   const age = calcAge(birthYear);
//   return `${firstName} retires in ${65 - age} years.`;
// };
// console.log(yearsUntilRetirement(1975, 'Andruxa'));

// const friends = ['Michael', 'Steven', 'Peter'];
// console.log(friends);
// const years = new Array(1991, 1984, 2008, 2020);
// console.log(years);

// console.log(friends.push('Andruxa'));

// console.log(friends);

// Exercise
// const calcAge = function (birthYear) {
//   return 2020 - birthYear;
// };

// const years = [1990, 1967, 2002, 2010, 2018];
// const ages = [];

// years.forEach((year) => {
//   ages.push(calcAge(year));
// });

// console.log(ages);

// const friends = ['Michael', 'Steven', 'Peter'];
// const newLength = friends.push('Jay');
// console.log(friends, newLength);
// friends.unshift('John');
// console.log(friends);

// friends.pop();
// console.log(friends);

// friends.shift();
// console.log(friends);

// console.log(friends.indexOf('Steven'));
// console.log(friends.indexOf('Bob'));

// console.log(friends.includes('Steven'));
// console.log(friends.includes('Bob'));
/*
const jonas = {
  firstName: 'Jonas',
  lastName: 'Schliemann',
  birthYear: 1991,
  job: 'teacher',
  friends: ['Michael', 'Peter', 'Steven'],
  hasDriversLicense: true,
  calcAge: function (birthYear = this.birthYear) {
    this.age = 2020 - birthYear;
    return this.age;
  },
  summary: function () {
    return `${this.firstName} is a ${this.calcAge()}-years old ${
      this.job
    }, and he has ${this.hasDriversLicense ? 'a' : 'no'} driver's license.`;
  },
};

jonas.location = 'Portugal';
jonas.town = 'Lisbon';

// console.log(jonas['calcAge']());
// console.log(jonas.age);
console.log(jonas.summary());
*/
/*
for (let rep = 1; rep <= 10; rep++) {
  console.log(`Lifting weight repetition ${rep}`);
}
*/
/*
const jonasArray = [
  'Jonas',
  'Schmitmann',
  2037 - 1991,
  'teacher',
  ['Michael', 'Peter', 'Steven'],
];

const typeJonas = [];
for (let i = 0; i < jonasArray.length; i++) {
  console.log(jonasArray[i]);
  typeJonas.push(typeof jonasArray[i]);
}
console.log(typeJonas);

typeJonas.length = 0;
jonasArray.forEach((element) => {
  console.log(element);
  typeJonas.unshift(typeof element);
});
console.log(typeJonas);
*/
/*
const jonas = [
  'Jonas',
  'Schmitmann',
  2037 - 1991,
  'teacher',
  ['Michael', 'Peter', 'Steven'],
];

for (let i = 0; i < jonas.length; i++) {
  if (typeof jonas[i] !== 'string') continue;
  console.log(jonas[i]);
}
*/
/*
const jonas = [
  'Jonas',
  'Schmitmann',
  2037 - 1991,
  'teacher',
  ['Michael', 'Peter', 'Steven'],
  true,
];

for (let i = jonas.length - 1; i >= 0; i--) {
  console.log(i, jonas[i]);
}

for (let i = 1; i <= 3; i++) {
  console.log('Exercise No.' + i);
  for (let j = 1; j <= 5; j++) {
    console.log('\tRepetition: ' + j);
  }
}

let i = 0;
while (i < jonas.length) {
  console.log(`Repetition: ${i}`, jonas[i]);
  i++;
}
*/
/*
let dice = Math.trunc(Math.random() * 6) + 1;
while (dice !== 6) {
  console.log(dice);
  dice = Math.trunc(Math.random() * 6) + 1;
  if (dice === 6) console.log(`We've got 6 on dice!`);
}
*/
