'use strict';
/*
console.log(document.querySelector('.message').textContent);

document.querySelector('.number').textContent = 13;
document.querySelector('.score').textContent = 15;

document.querySelector('.guess').value = 23;
console.log(document.querySelector('.guess').value);
*/
// Initialize
let score = Number();
let highscore = 0;
let secretNumber = Number();
const getRandNumber = () => Math.trunc(Math.random() * 20) + 1;

const displayMessage = (msg) =>
  (document.querySelector('.message').textContent = msg);

function initialize() {
  score = 20;
  secretNumber = getRandNumber();
  // initialize UI
  document.querySelector('body').style.backgroundColor = '#222';
  document.querySelector('.number').style.width = '15rem';
  document.querySelector('.number').textContent = '?';
  document.querySelector('.number').textContent = secretNumber; // TODO delete when done!
  displayMessage('Guess the number...');
  document.querySelector('.score').textContent = score;
  document.querySelector('.highscore').textContent = highscore;
  document.querySelector('.guess').value = '';
}

initialize();

document.querySelector('.check').addEventListener('click', function () {
  const guess = Number(document.querySelector('.guess').value);

  // Player enters no number
  if (!guess) {
    displayMessage('No number!');

    // Player enters correct number
  } else if (guess === secretNumber && score > 1) {
    displayMessage('Correct!');
    document.querySelector('.number').textContent = secretNumber;
    document.querySelector('body').style.backgroundColor = '#60b347';
    document.querySelector('.number').style.width = '30rem';
    highscore = Number(document.querySelector('.highscore').textContent);
    if (highscore < score) highscore = score;
    // Player guess wrong number
  } else if (guess !== secretNumber) {
    if (score > 1) {
      displayMessage(
        guess < secretNumber ? 'Number is higher!' : 'Number is lower!'
      );
      score--;
      document.querySelector('.score').textContent = score;
    } else {
      displayMessage('You lost the game!');
      document.querySelector('.score').textContent = '0';
    }
  }
});

document.querySelector('.again').addEventListener('click', function () {
  initialize();
});
