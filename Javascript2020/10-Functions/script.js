'use strict';

// const bookings = [];

// const createBooking = function (
//   flightNum,
//   numPassengers = 1,
//   price = 199 * numPassengers
// ) {
//   // ES5
//   // numPassengers = numPassengers || 1;
//   // price = price || 199;

//   const booking = {
//     flightNum,
//     numPassengers,
//     price,
//   };
//   console.log(booking);
//   bookings.push(booking);
// };

// // createBooking('LH123');
// // createBooking('LH222', undefined, 1000);
// // createBooking('LH222', 5);

// // --- Primitive vs Reference

// const flight = 'LH234';
// const jonas = {
//   name: 'Jonas Scmedtmann',
//   passport: 44323214506954,
// };

// const checkIn = function (flightNum, passenger) {
//   flightNum = 'LH999';
//   passenger.name = 'Mr. ' + passenger.name;

//   if (passenger.passport === 44323214506954) {
//     alert('Checked in');
//   } else {
//     alert('Wrong passport!');
//   }
// };

// // checkIn(flight, jonas);
// // console.log(flight);
// // console.log(jonas);

// const newPassport = function (person) {
//   person.passport = Math.trunc(Math.random() * 100000000000);
//   console.log(person.passport);
// };
// /*
// console.log(jonas.passport);
// newPassport(jonas);
// console.log(jonas.passport);
// checkIn(flight, jonas);
// console.log(flight, jonas);
// */

// // Higher-order functions

// const oneWord = function (str) {
//   return str.replace(/ /g, '').toLowerCase();
// };

// const upperFirstWord = function (str) {
//   const [first, ...others] = str.split(' ');
//   return `${first.toUpperCase()} ${others.join(' ')}`;
// };

// const transformer = function (str, fn) {
//   console.log(`Original string: ${str}`);
//   console.log(`Transformed string: ${fn(str)}`);
//   console.log(`Name of the callback function: ${fn.name}`);
// };

// // transformer('Javascript is the best!', upperFirstWord);
// transformer('Javascript is the best!', oneWord);

// const high5 = function (value, key) {
//   console.log(`${key} 👋 ${value}`);
// };
// /*
// document.body.addEventListener('click', high5);
// ['Jonas', 'Adam', 'Martha'].forEach(high5);
// */
// // Closures

// const greet = (greeting) => (name) => `${greeting} ${name}`;

// const greeterName = greet('Hey');
// console.log(greeterName('Andruxa'));

// console.log(greet('Hello')('Jonas'));

// //

// const lufthansa = {
//   airline: 'Lufthansa',
//   iataCode: 'LH',
//   bookings: [],
//   book(flightNum, name) {
//     console.log(
//       `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`
//     );
//     this.bookings.push({ flight: this.iataCode + flightNum, name });
//   },
// };

// lufthansa.book(234, 'Jonas Schmedtmann');
// lufthansa.book(443, 'Adam Sandler');
// console.log(lufthansa);

// const eurowings = {
//   airline: 'Eurowings',
//   iataCode: 'EW',
//   bookings: [],
// };

// // Call method

// lufthansa.book.call(eurowings, 123, 'Sarah Williams');
// console.log(eurowings);
// lufthansa.book.call(lufthansa, 567, 'Alan Parker');
// console.log(lufthansa);

// // Bind method
// const bookEurowings = lufthansa.book.bind(eurowings);
// bookEurowings(789, 'John Smith');
// console.log(eurowings);

// lufthansa.planes = 300;
// lufthansa.buyPlane = function () {
//   this.planes += 1;
//   console.log(this);
//   console.log(this.planes);
// };

// document
//   .querySelector('.buy')
//   .addEventListener('click', lufthansa.buyPlane.bind(lufthansa));

// // Partial application
// /*
// const addTax = (rate, value) => value + value * rate;
// console.log(addTax(0.21, 100));
// const taxVAT = addTax.bind(null, 0.21);
// console.log(taxVAT(200));
// */

// // const addTax = function (rate) {
// //   return function (value) {
// //     return value + value * rate;
// //   };
// // };

// const addTax = (rate) => (value) => value + value * rate;

// const taxVAT21 = addTax(0.21);
// console.log(taxVAT21(100));

// Closures

const boardPassenger = function (n, wait) {
  const perGroup = n / 3;
  setTimeout(() => {
    console.log(`We are now boarding all ${n} passenger`);
    console.log(`There are 3 groups, each with ${perGroup} passenger`);
  }, wait * 1000);
  console.log(`Will start boarding in ${wait} seconds`);
};

boardPassenger(180, 3);
