'use strict';

const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

const openingHours = {
  [weekdays[3]]: {
    open: 12,
    close: 22,
  },
  [weekdays[4]]: {
    open: 11,
    close: 23,
  },
  sat: {
    open: 0, // Open 24 hours
    close: 24,
  },
};

const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours,

  orderDelivery: function ({
    starterIndex = 0,
    mainIndex = 1,
    time = '20:00',
    address,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`
    );
  },
  orderPasta: function (ing1, ing2, ing3) {
    console.log(ing1, ing2, ing3);
  },

  orderPizza: function (main, ...others) {
    console.log(main, others);
  },

  order(ing1, ing2) {
    return [this.starterMenu[ing1], this.mainMenu[ing2]];
  },
};

const days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
for (const day of days) {
  console.log(
    `On ${day} we are open at ${openingHours[day]?.open ?? 'closed'}`
  );
}

console.log(restaurant?.order(0, 1) ?? 'Method not exists');

const users = [{ name: 'Jonas', email: 'hello@jonas.io' }];
// const users = [];
console.log(users[0]?.name ?? 'Users array is empty');

if (users.length > 0) console.log(users[0].name);
else console.log('Array is empty!!!');

// for each
for (const value of Object.values(openingHours)) {
  console.log(value.open);
}

//
const entries = Object.entries(openingHours);
// console.log(entries);
for (const [key, { open, close }] of entries) {
  // console.log(`On ${key} we open at ${open} and close at ${close}`);
  console.log(key, open, close);
}

// console.log(restaurant?.openingHours?.fri?.open);

/*
const ingredients = [
  // prompt('Pasta ingredient 1:'),
  // prompt('Pasta ingredient 2:'),
  // prompt('Pasta ingredient 3:'),
];
// restaurant.orderPasta(...ingredients);

// Objects shallow copy
const newRestaurant = { foundedIn: 1998, ...restaurant, founder: 'Gouiseppe' };
console.log(newRestaurant);

restaurant.orderDelivery({
  time: '22:30',
  address: 'Via Del Sole, 21',
  starterIndex: 2,
  mainIndex: 2,
});

restaurant.orderDelivery({
  starterIndex: 3,
  address: 'Lielais prospekts 22-18',
});

// Spreading array
const arr = [7, 8, 9];
const newArr = [1, 2, ...arr];
console.log(newArr);
const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log(menu);

// REST operator
const [a, b, ...others] = [1, 2, 3, 4, 5];
console.log(a, b, others);

const { fri, ...weekDays } = restaurant.openingHours;
console.log(fri, weekDays);

const addNum = function (...numbers) {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) sum += numbers[i];
  return sum;
};

console.log(addNum(2, 3, 4, 5, 6));
const x = [23, 4, 5];
console.log(addNum(...x));

restaurant.orderPizza('mushrooms', 'cheese', 'salami');

// Object destructuring
const { name, categories, openingHours } = restaurant;
console.log(name, categories, openingHours);

const { name: restName, categories: tags, openingHours: hours } = restaurant;
console.log(restName, tags, hours);

// Mutating variables
// let a = 111;
// let b = 999;
// const obj = { a: 23, b: 7, c: 94 };
// ({ a, b } = obj);
// console.log(a, b);

// Destructuring nested objects
const {
  fri: { open: o, close: c },
} = openingHours;
console.log('OPEN', o, '-', c);

// Destructuring arrays
// const arr = [2, 3, 4];

let [main, , secondary] = restaurant.categories;
console.log(main, secondary);
[main, secondary] = [secondary, main];
console.log(main, secondary);

// Short-circuiting
console.log(false || 'Defined');

console.log(true && 'Defined22');

console.log(0 ?? 10);
*/
const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log(menu);

for (const [index, value] of menu.entries()) {
  console.log(`${index + 1}: ${value}`);
}

// Sets
const setA = new Set('AndruxA');
console.log(setA);
console.log(setA.has('o'));
setA.delete('x');
setA.add('ddd');
console.log(setA);

for (const order of setA) console.log(order);

const staff = ['Chef', 'Waiter', 'Manager', 'Waiter', 'Chef'];
const staffUnique = new Set(staff);
console.log(staffUnique);
const arrStaff = [...staffUnique];
console.log(arrStaff);

// Maps
const rest = new Map();
rest.set(true, 'We are open :)').set(false, 'We are closed :(');
rest.set('open', 9).set('close', 23);

const time = 3;
console.log(rest.get('open'), rest.get('close'));
console.log(rest.get(time > rest.get('open') && time < rest.get('close')));

rest.set(document.querySelector('h1'), 'Heading');

console.log(rest);

const question = new Map([
  ['question', 'What is best programming language in the world?'],
  [1, 'C'],
  [2, 'Java'],
  [3, 'Javascript'],
  ['correct', 3],
  [true, 'Correct!'],
  [false, 'Try again!'],
]);
console.log(question);

const opnHrsMap = new Map(Object.entries(openingHours));
console.log(opnHrsMap);

console.log(question.get('question'));
for (const [key, value] of question) {
  if (typeof key === 'number') console.log(`Answer ${key}: ${value}`);
}
const answer = 3;
console.log(question.get(question.get('correct') === answer));

// Convert Map to an array
console.log([...question]);
console.log([...question.keys()]);
console.log([...question.values()]);

// Strings

const airline = 'TAP Air Portugal';
const plane = 'A320';

console.log(airline.slice(0, airline.indexOf(' ')));
console.log(airline.slice(airline.lastIndexOf(' ') + 1));

const checkMiddleSeat = function (seat) {
  // B and E are middle seats
  const s = seat.slice(-1);
  console.log(
    `Seat ${seat} is ${
      s === 'B' || s === 'E' ? 'a middle' : 'NOT a middle'
    } seat.`
  );
};

checkMiddleSeat('11B');
checkMiddleSeat('23C');
checkMiddleSeat('3E');

console.log(airline.toLowerCase());
const passenger = 'jOnAS';
console.log(passenger.toUpperCase()[0] + passenger.slice(1).toLowerCase());

// Comparing emails
const email = 'hello@jonas.io';
const loginEmail = '   Hello@Jonas.io  \n';
console.log(
  loginEmail.toLowerCase().trim() === email ? 'Correct email' : 'Wrong email'
);

// Replacing part of strings
const announcement =
  'All passengers come to boarding door 23. Boarding door 23!';
console.log(announcement.replace('door', 'gate'));
console.log(announcement.replace(/door/g, 'gate'));

console.log(announcement.split(' '));

const [firstName, lastName] = 'Jonas Schmitmann'.split(' ');
console.log(['Mr.', firstName, lastName.toUpperCase()].join(' '));

const capitalizeName1 = function (name) {
  const arrNames = [];
  for (const word of name.toLowerCase().split(' ')) {
    arrNames.push(word[0].toUpperCase() + word.slice(1));
  }
  return arrNames.join(' ');
};

const capitalizeName2 = function (name) {
  const arrNames = [];
  for (const word of name.toLowerCase().split(' ')) {
    arrNames.push(word.replace(word[0], word[0].toUpperCase()));
  }
  return arrNames.join(' ');
};

const passenger1 = 'jessica ann smith davis';
console.log(capitalizeName2(passenger1));
console.log(capitalizeName2('jOnaS sChmedmann'));

// Padding
const maskCreditCard = function (number) {
  const str = number + '';
  return str.slice(-4).padStart(str.length, '*');
};

console.log(maskCreditCard(343234));
console.log(maskCreditCard('fefefe33333455554'));

// Repeat
const message = 'Bad weather... All departures delayed... ';
console.log(message.repeat(5));
