'use strict';

// OBJECTS

const Person = function (firstName, birthYear) {
  this.firstName = firstName;
  this.birthYear = birthYear;
};

const jonas = new Person('Jonas', 1989);

console.log(jonas);

// 1. New {} is created
// 2. function is called, this = {}
// 3. {} linked to prototype
// 4. function automatically return {}

const matilda = new Person('Matilda', 1991);
const jack = new Person('Jack', 1995);

console.log(matilda, jack);
console.log(jonas instanceof Person);

// PROTOTYPES
console.log(Person.prototype);

Person.prototype.calcAge = function () {
  return 2021 - this.birthYear;
};

console.log(jonas.calcAge());
console.log(jonas.__proto__);
console.log(Person.prototype);
console.log(Person.prototype.isPrototypeOf(matilda));

Person.prototype.species = 'Homo Sapiens';
console.log(jonas.species, matilda.species);

console.log(jonas.hasOwnProperty('firstName'));
console.log(jonas.hasOwnProperty('species'));

const arr = [2, 3545, 5, 5, 4, 3, 2, 5];
console.log(arr.__proto__);

class PersonCl {
  constructor(fullName, birthYear) {
    this.fullName = fullName;
    this.birthYear = birthYear;
  }
  calcAge() {
    return 2021 - this.birthYear;
  }

  get age() {
    return 2021 - this.birthYear;
  }

  // Set property that already exists
  set fullName(name) {
    if (name.includes(' ')) this._fullName = name;
    else alert(`${name} IS NOT A FULL NAME!`);
  }
  get fullName() {
    return this._fullName;
  }
}

const jessica = new PersonCl('Jessica Davis', 1991);
console.log(jessica.fullName);
console.log(jessica);

const walter = new PersonCl('Walter Shchmidt', 1965);

const account = {
  owner: 'Jonas',
  movements: [200, 432, 343, 234],
  get latest() {
    return this.movements.slice(-1).pop();
  },
  set latest(mov) {
    this.movements.push(mov);
  },
};

console.log(account.latest);
account.latest = 50;
console.log(account.movements);

/////

const Pers = function (firstName, birthYear) {
  this.firstName = firstName;
  this.birthYear = birthYear;
};

Pers.prototype.calcAge = function () {
  console.log(2021 - this.birthYear);
};

const Student = function (firstName, birthYear, course) {
  Pers.call(this, firstName, birthYear);
  this.course = course;
};

Student.prototype = Object.create(Pers.prototype);

Student.prototype.introduce = function () {
  console.log(`My name is ${this.firstName} and I study ${this.course}.`);
};

const john = new Student('John', 1975, 'Computer Science');

john.introduce();
john.calcAge();

//
class PersCl {
  constructor(fullName, birthYear) {
    this.fullName = fullName;
    this.birthYear = birthYear;
  }
  calcAge() {
    return 2021 - this.birthYear;
  }

  get age() {
    return 2021 - this.birthYear;
  }

  // Set property that already exists
  set fullName(name) {
    if (name.includes(' ')) this._fullName = name;
    else alert(`${name} IS NOT A FULL NAME!`);
  }
  get fullName() {
    return this._fullName;
  }
}

class StudCl extends PersCl {
  constructor(fullName, birthYear, course) {
    super(fullName, birthYear);
    this.course = course;
  }
  introduce() {
    console.log(`My name is ${this.fullName} and I study ${this.course}`);
  }
  calcAge() {
    console.log(
      `I am ${
        2021 - this.birthYear
      } years old, but as a student I feel more like ${
        2021 - this.birthYear + 10
      } years old!`
    );
  }
}

const martha = new StudCl('Martha Jones', 1982, 'Computer Science');
martha.fullName = 'Martha Pipkus';
console.log(martha.fullName);
martha.introduce();
martha.calcAge();

const PersonProto = {
  init(fullName, birthYear) {
    this.fullName = fullName;
    this.birthYear = birthYear;
  },
  calcAge() {
    console.log(2021 - this.birthYear);
  },
};

// const steven = Object.create(PersonProto);

const StudentProto = Object.create(PersonProto);
StudentProto.init = function (fullName, birthYear, course) {
  PersonProto.init.call(this, fullName, birthYear);
  this.course = course;
};

const jay = Object.create(StudentProto);
jay.init('Jay Hopkins', 1975, 'Computer Science');
