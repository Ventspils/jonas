'use strict';

const me = {
  name: 'Jonas',
  age: 30,
};

const friend = me;

friend.age = 27;

console.log('Friend', friend);
console.log('Me', me);
