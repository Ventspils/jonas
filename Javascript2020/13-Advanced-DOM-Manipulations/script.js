'use strict';

///////////////////////////////////////
// Modal window

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');

const openModal = function (e) {
  e.preventDefault();
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

btnsOpenModal.forEach((btn) => btn.addEventListener('click', openModal));

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});
///////////////////////////////////////
// Page Navigation
// document.querySelectorAll('.nav__link').forEach(function (el) {
//   el.addEventListener('click', function (e) {
//     e.preventDefault();
//     const id = this.getAttribute('href');
//     console.log(id);
//     document.querySelector(id).scrollIntoView({ behavior: 'smooth' });
//   });
// });

document.querySelector('.nav__links').addEventListener('click', function (e) {
  e.preventDefault();

  if (e.target.classList.contains('nav__link')) {
    const id = e.target.getAttribute('href');
    document.querySelector(id).scrollIntoView({ behavior: 'smooth' });
  }
});

// Tab navigation
const tabContainer = document.querySelector('.operations__tab-container');
const tabs = tabContainer.querySelectorAll('.operations__tab');

tabContainer.addEventListener('click', function (e) {
  const clicked = e.target.closest('.operations__tab');
  if (!clicked) return;
  // Make tab active
  tabs.forEach((t) => t.classList.remove('operations__tab--active'));
  clicked.classList.add('operations__tab--active');
  // Show tab content
  document // Remove --active class from all contents
    .querySelectorAll('.operations__content')
    .forEach((c) => c.classList.remove('operations__content--active'));
  document // Add --active class to selected dataset tab
    .querySelector(`.operations__content--${clicked.dataset.tab}`)
    .classList.add('operations__content--active');
});

// Fade effect
const nav = document.querySelector('.nav');
const fadeNav = function (e) {
  const link = e.target;
  if (link.classList.contains('nav__link')) {
    link
      .closest('.nav')
      .querySelectorAll('.nav__link')
      .forEach((el) => {
        el.style.opacity = this;
      });
  }
  link.style.opacity = 1;
};

nav.addEventListener('mousemove', fadeNav.bind(0.5));
nav.addEventListener('mouseout', fadeNav.bind(1));

// Sticky menu
const sect1 = document.querySelector('#section--1');
// const initialCoords = sect1.getBoundingClientRect();
// console.log(initialCoords);

// window.addEventListener('scroll', function () {
//   if (window.scrollY > initialCoords.top) nav.classList.add('sticky');
//   else nav.classList.remove('sticky');
// });

// -- Intersection Observer API
// const obsCallback = function (entries, observer) {
//   entries.forEach((entry) => console.log(entry));
// };
// const obsOptions = {
//   root: null,
//   threshold: 0.1,
// };

// const observer = new IntersectionObserver(obsCallback, obsOptions);
// observer.observe(sect1);
const headerObs = document.querySelector('.header');
const navHeight = nav.getBoundingClientRect().height;

const headerCallback = function (entries) {
  const [entry] = entries;
  if (!entry.isIntersecting) nav.classList.add('sticky');
  else nav.classList.remove('sticky');
};

const headerObserver = new IntersectionObserver(headerCallback, {
  root: null,
  threshold: 0,
  rootMargin: `-${navHeight}px`,
});
headerObserver.observe(headerObs);

// Reveal sections
const allSections = document.querySelectorAll('.section');

const revealSection = function (entries, observer) {
  const [entry] = entries;

  if (!entry.isIntersecting) return;
  entry.target.classList.remove('section--hidden');
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.15,
});

allSections.forEach(function (section) {
  // sectionObserver.observe(section);
  // section.classList.add('section--hidden');
});

// Lazy loaded images
const imgTargets = document.querySelectorAll('img[data-src]');

const loadImg = function (entries, observer) {
  const [entry] = entries;
  console.log(entry);
  if (!entry.isIntersecting) return;

  entry.target.src = entry.target.dataset.src;
  entry.target.addEventListener('load', function () {
    entry.target.classList.remove('lazy-img');
  });
  observer.unobserve(entry.target);
};

const imgObserver = new IntersectionObserver(loadImg, {
  root: null,
  threshold: 0,
});

imgTargets.forEach((img) => imgObserver.observe(img));

// Slider
const slides = document.querySelectorAll('.slide');
const btnLeft = document.querySelector('.slider__btn--left');
const btnRight = document.querySelector('.slider__btn--right');
let curSlide = 0;
const maxSlides = slides.length;
const dotsContainer = document.querySelector('.dots');

// const slider = document.querySelector('.slider');
// slider.style.transform = 'scale(0.4) translateX(-800px)';
// slider.style.overflow = 'visible';

const createDots = function () {
  slides.forEach(function (_, i) {
    dotsContainer.insertAdjacentHTML(
      'beforeend',
      `<button class="dots__dot" data-slide="${i}"></button>`
    );
  });
};
createDots();

const activateDot = function (slide) {
  document.querySelectorAll('.dots__dot').forEach(function (dot) {
    dot.classList.remove('dots__dot--active');
  });
  document
    .querySelector(`.dots__dot[data-slide="${slide}"]`)
    .classList.add('dots__dot--active');
};

const goToSlide = function (slide) {
  slides.forEach(function (s, i) {
    s.style.transform = `translateX(${100 * (i - slide)}%)`;
  });
};

const nextSlide = function () {
  if (curSlide === maxSlides - 1) {
    curSlide = 0;
  } else {
    curSlide++;
  }
  goToSlide(curSlide);
  activateDot(curSlide);
};

const prevSlide = function () {
  if (curSlide === 0) {
    curSlide = maxSlides - 1;
  } else {
    curSlide--;
  }
  goToSlide(curSlide);
  activateDot(curSlide);
};

goToSlide(0);
activateDot(0);

//Next slide
btnRight.addEventListener('click', nextSlide);
// Previous slide
btnLeft.addEventListener('click', prevSlide);

document.addEventListener('keydown', function (e) {
  if (e.key === 'ArrowLeft') prevSlide();
  e.key === 'ArrowRight' && nextSlide();
});

dotsContainer.addEventListener('click', function (e) {
  if (e.target.classList.contains('dots__dot')) {
    const { slide } = e.target.dataset;
    goToSlide(slide);
    activateDot(slide);
  }
});

///////////////////////////////////////
// LECTURES
// const allSections = document.querySelectorAll('.section');
// allSections.forEach((sect) => {
//   // sect.remove();
// });

// Creating and inserting elements
const header = document.querySelector('.header');
const message = document.createElement('div');
// message.classList.add('cookie-message');

message.innerHTML =
  'This is test message <button class="btn btn--close-cookie">Got it</button>';
header.append(message);
message.querySelector('.btn--close-cookie').addEventListener('click', () => {
  message.remove();
  // message.parentElement.removeChild(message);
});
message.style.backgroundColor = '#37383d';
message.style.width = '120%';
message.style.height =
  Number.parseFloat(getComputedStyle(message).height, 10) + 30 + 'px';

console.log(getComputedStyle(message).height);
const logo = document.querySelector('.nav__logo');
logo.setAttribute(
  'src',
  'https://image.spreadshirtmedia.com/image-server/v1/mp/compositions/T127A1MPA215PT21X7Y6D1026362631FS594/views/1,width=550,height=550,appearanceId=1,backgroundColor=FFFFFF,noPt=true,version=1574256941/eat-pussy-small-buttons.jpg'
);
logo.setAttribute('width', '100px');
logo.setAttribute('height', '100px');

// Scrolling
const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');

btnScrollTo.addEventListener('click', function (e) {
  const s1coords = section1.getBoundingClientRect();
  // window.scrollTo(
  //   window.pageXOffset + s1coords.left,
  //   window.pageYOffset + s1coords.top
  // );
  // window.scrollTo({
  //   left: window.pageXOffset + s1coords.left,
  //   top: window.pageYOffset + s1coords.top,
  //   behavior: 'smooth',
  // });

  section1.scrollIntoView({ behavior: 'smooth' });
});

// Events
const elH1 = document.querySelector('h1');

const fnAlert = function (e) {
  alert('H1 enetered');
};

// elH1.onmouseenter = fnAlert;

// elH1.addEventListener('mouseenter', fnAlert);
// setTimeout(function () {
//   elH1.removeEventListener('mouseenter', fnAlert);
// }, 5000);

// Bubbling
// const randInt = function (min, max) {
//   return Math.trunc(Math.random() * (max - min + 1) + min);
// };
// console.log(randInt(0, 3));
// const randColor = function () {
//   return `rgb(${randInt(0, 255)},${randInt(0, 255)},${randInt(0, 255)})`;
// };
// console.log(randColor());
// elH1.setAttribute('color', randColor());

// document.querySelector('.nav__link').addEventListener('click', function () {
//   this.style.backgroundColor = randColor();
// });

// document.querySelector('.nav__links').addEventListener('click', function (e) {
//   this.style.backgroundColor = randColor();
//   console.log('ddd', e.target);
// });

// document.querySelector('.nav').addEventListener('click', function () {
//   this.style.backgroundColor = randColor();
// });

// DOM traversing
// Going downwards: child
const h1 = document.querySelector('h1');
console.log(h1.querySelectorAll('.highlight'));
console.log(h1.childNodes);
console.log(h1.children);
console.log(h1.lastElementChild);

// Going upwards: parents
console.log(h1.parentNode);
console.log(h1.parentElement);

h1.closest('.header').style.background = 'var(--gradient-secondary)';
h1.closest('h1').style.background = 'var(--gradient-primary)';

[...h1.parentElement.children].forEach(function (el) {
  if (el !== h1) el.style.transform = 'scale(0.5)';
});
