'use strict';
// Defining html elements
const elPlayer0 = document.querySelector('.player--0');
const elPlayer1 = document.querySelector('.player--1');
const elScore0 = document.querySelector('#score--0');
const elScore1 = document.getElementById('score--1');
const elCurScore0 = document.getElementById('current--0');
const elCurScore1 = document.getElementById('current--1');
const elDice = document.querySelector('.dice');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');
// Defining global variables
const score = Array();
let currentScore = Number();
let activePlayer = Number();
let gameOver = Boolean();

const initialize = function () {
  // Resetting global variables
  activePlayer = currentScore = score[0] = score[1] = gameOver = 0;
  // Resetting element's textContent
  elScore0.textContent = elScore1.textContent = elCurScore0.textContent = elCurScore1.textContent = 0;
  // Resetting elements classes to initial state
  elDice.classList.add('hidden');
  elPlayer0.className = 'player player--0 player--active';
  elPlayer1.className = 'player player--1';
};

const switchPlayers = function () {
  document.getElementById(
    `current--${activePlayer}`
  ).textContent = currentScore = 0;
  // elDice.classList.add('hidden');
  // console.log(score);
  elPlayer0.classList.toggle('player--active');
  elPlayer1.classList.toggle('player--active');
  activePlayer = activePlayer === 0 ? 1 : 0;
};

initialize();

// Rolling dice functionality
btnRoll.addEventListener('click', function () {
  if (!gameOver) {
    // 1. Generate a random dice
    const dice = Math.trunc(Math.random() * 6) + 1;
    // console.log(dice);

    // 2. Display dice
    if (elDice.classList.contains('hidden')) elDice.classList.remove('hidden');
    elDice.src = `dice-${dice}.png`;

    // 3. Check if dice either 1: if true, switch to other player
    if (dice !== 1) {
      // Add to current score
      document.getElementById(
        `current--${activePlayer}`
      ).textContent = currentScore += dice;
    } else {
      switchPlayers();
    }
  }
});

btnHold.addEventListener('click', function () {
  if (!gameOver) {
    document.getElementById(`score--${activePlayer}`).textContent = score[
      activePlayer
    ] += currentScore;
    // If Player wins
    if (score[activePlayer] >= 100) {
      gameOver = true;
      document.getElementById(
        `current--${activePlayer}`
      ).textContent = currentScore = 0;
      document.querySelector('.dice').classList.add('hidden');
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.remove('player--active');
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.add('player--winner');
    } else {
      if (currentScore !== 0) switchPlayers();
    }
  }
});

btnNew.addEventListener('click', function () {
  initialize();
});
