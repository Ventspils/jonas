'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

const printMovements = function (movements, sorted = false) {
  containerMovements.innerHTML = '';

  const movs = sorted ? movements.slice().sort((a, b) => a - b) : movements;

  movs.forEach((mov, i) => {
    const type = mov > 0 ? 'deposit' : 'withdrawal';
    const html = `
      <div class="movements__row">
        <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
        <div class="movements__value">${mov}€</div>
      </div>
    `;
    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

const displayBalance = function (account) {
  account.balance = account.movements.reduce((prev, curr) => prev + curr);
  labelBalance.textContent = `${account.balance} EUR`;
};

const displaySummary = function (account) {
  labelSumIn.textContent =
    account.movements.filter((mov) => mov > 0).reduce((acc, mov) => acc + mov) +
    '€';
  labelSumOut.textContent =
    Math.abs(
      account.movements.filter((mov) => mov < 0).reduce((acc, mov) => acc + mov)
    ) + '€';
  labelSumInterest.textContent =
    account.movements
      .filter((mov) => mov > 0)
      .map((deposit) => (deposit * account.interestRate) / 100)
      .filter((int) => int >= 1) // only add interest > 1.00 eur
      .reduce((acc, int) => acc + int) + '€';
};

const createUsernames = (accs) => {
  accs.forEach((acc) => {
    acc.username = acc.owner
      .toLowerCase()
      .trim()
      .split(' ')
      .map((usr) => usr[0])
      .join('');
  });
};
createUsernames(accounts);

const updateUI = function (account) {
  // Display movements
  printMovements(account.movements);
  // Display balance
  displayBalance(account);
  // Display summary
  displaySummary(account);
};

// SET INITIAL VALUES
let currentAccount;
let sorted = false;

// LOGIN
btnLogin.addEventListener('click', function (e) {
  e.preventDefault();
  currentAccount = accounts.find(
    (acc) => acc.username === inputLoginUsername.value
  );

  if (currentAccount?.pin === Number(inputLoginPin.value)) {
    //Clear login fields
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();
    //Display UI and message
    containerApp.style.opacity = 100;
    labelWelcome.textContent = `Welcome back, ${
      currentAccount.owner.split(' ')[0]
    }`;
    updateUI(currentAccount);
  }
});

// TRANSFER FUNDS
btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = Number(inputTransferAmount.value);
  const transferAccTo = accounts.find(
    (acc) => acc.username === inputTransferTo.value
  );
  //Clear transfer fields
  inputTransferAmount.value = inputTransferTo.value = '';

  if (
    transferAccTo &&
    transferAccTo !== currentAccount &&
    amount > 0 &&
    currentAccount.balance >= amount
  ) {
    // Updating account records
    currentAccount.movements.push(-amount);
    transferAccTo.movements.push(amount);
    updateUI(currentAccount);
  }
});

// REQUEST A LOAN
btnLoan.addEventListener('click', function (e) {
  e.preventDefault();
  const loanAmount = Number(inputLoanAmount.value);
  if (
    // loanAmount &&
    loanAmount > 0 &&
    currentAccount.movements.some((mov) => mov * 0.1 >= loanAmount)
  ) {
    console.log('Loan granted');
    currentAccount.movements.push(loanAmount);
    updateUI(currentAccount);
  } else {
    console.log('Loan not granted');
  }
});

// CLOSE ACCOUNT
btnClose.addEventListener('click', function (e) {
  e.preventDefault();
  if (
    currentAccount.username === inputCloseUsername.value &&
    currentAccount.pin === Number(inputClosePin.value)
  ) {
    const deleteIndex = accounts.findIndex(
      (acc) => acc.username === currentAccount.username
    );
    accounts.splice(deleteIndex, 1);
    containerApp.style.opacity = 0;
    inputCloseUsername.value = inputClosePin.value = '';
  }
});

// SORT MOVEMENTS
btnSort.addEventListener('click', function (e) {
  e.preventDefault();
  printMovements(currentAccount.movements, !sorted);
  sorted = !sorted;
  console.log('Sorted!');
});

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

// const currencies = new Map([
//   ['USD', 'United States dollar'],
//   ['EUR', 'Euro'],
//   ['GBP', 'Pound sterling'],
// ]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
/*
/////////////////////////////////////////////////
// SLICE
let arr = ['a', 'b', 'c', 'd', 'e'];
console.log(arr.slice());
console.log([...arr].slice(-1));
// SPLICE
console.log(arr.splice(-2));
console.log(arr);
// REVERSE
arr = ['a', 'b', 'c', 'd', 'e'];
const arr2 = ['j', 'i', 'h', 'g', 'f'];
arr2.reverse();
console.log(arr2);
// CONCAT
const letters = arr.concat(arr2);
console.log(letters);
console.log([...arr, ...arr2]);
// JOIN
console.log(letters.join(' - '));
/////////////////////////////////////////////////
*/
/*
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
movements.forEach((value) =>
  console.log(value < 0 ? `Withdrawal of ${value}` : `Deposit of ${value}`)
);
*/
/*
const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

currencies.forEach((value, key, map) => {
  console.log(`${key}: ${value}`);
});

const currenciesUnique = new Set(['USD', 'EUR', 'GBP', 'USD', 'EUR']);
currenciesUnique.forEach((value, _, set) => {
  console.log(`${value}: ${value}`);
});
*/
/*
const movementsUSD = movements.map((mov) => mov * 1.1);
console.log(movementsUSD);

const movementsDescription = movements.map((mov, i, arr) => {
  return `${i + 1}: You ${mov < 0 ? `withdrew` : `deposited`} ${Math.abs(mov)}`;
});
console.log(movementsDescription);
*/
/*
const deposits = movements.filter((mov) => mov > 0);
console.log(movements);
console.log(deposits);

const withdrawals = movements.filter((mov) => mov < 0);
console.log(withdrawals);

const balance = movements.reduce((prev, curr) => {
  return prev + curr;
}, 160);
console.log(balance);

console.log(movements.reduce((prev, curr) => (curr > prev ? curr : prev)));

console.log(accounts.find((acc) => acc.owner === 'Jessica Davis'));

for (const acc of accounts) {
  if (acc.owner === 'Jessica Davis') {
    console.log(acc);
  }
}
*/

const allAccountsMovements = accounts
  .map((acc) => acc.movements)
  .flat()
  .reduce((acc, cur) => acc + cur, 0);
// console.log(allAccountsMovements);

// console.log(movements);
// console.log(
//   movements.sort((a, b) => {
//     if (a < b) return 1;
//     if (a > b) return -1;
//   })
// );
// console.log(movements.sort((a, b) => a - b));

const arr = Array.from({ length: 10 }, () =>
  Math.trunc(Math.random() * 10 + 1)
);
// console.log(arr.sort((a, b) => a - b));

labelBalance.addEventListener('click', () => {
  const movementsDOM = Array.from(
    document.querySelectorAll('.movements__value'),
    (el) => Number(el.textContent.replace('€', ''))
  );
  console.log(document.querySelectorAll('.movements__value').textContent);
  console.log(movementsDOM);
});
