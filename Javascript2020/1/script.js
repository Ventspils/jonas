console.log('------ SCRIPT.JS ------');
// // let firstName = 'Jonas';
// let variable = 'ddd';
// variable = 2;
// console.log(typeof variable);

// let undef;
// console.log(typeof undef);
// console.log(typeof null);

// const currentYear = 2037;
// const ageJonas = currentYear - 1991;
// const ageSarah = currentYear - 2018;
// console.log(ageJonas, ageSarah);
// console.log(10 ** 3);
// const firstName = 'Jonas';
// const lastName = 'Schliemann';
// console.log(firstName + ' ' + lastName);

// // Assignment operators
// let x = 10 + 5;
// x++;
// x--;
// x--;
// console.log(x);

// // Comparison operators
//

// console.log('String with \n\
// multiple \
// lines');

// console.log(`String
// multi
// lines`);

// personAge = 24;
// const isOldEnough = personAge >= 18;

// if (!isOldEnough) {
//   console.log(
//     `Person is not allowed to drive yet. ${
//       18 - personAge
//     } years left until allowed.`
//   );
// } else {
//   console.log(`Yes you can apply for driver's license 🚗`);
// }

// const favNumber = Number(prompt('What is yours favorite number?'));
// console.log(typeof favNumber, favNumber);

// if (favNumber === 23) {
//   console.log('23 is Cool! number');
// } else if (favNumber === 7) {
//   console.log('7 is also cool number');
// } else {
//   console.log(`${favNumber} is not in desired list`);
// }

// if (favNumber !== 23) console.log('Why not a number 23???');

// const day = prompt('Enter week day');

// switch (day) {
//   case 'monday':
//     console.log('Goto gym');
//     break;

//   case 'tuesday':
//     console.log('Make homework');
//     break;

//   default:
//     console.log('Do nothing');
// }

// const age = 12;

// const drink = age >= 18 ? 'wine' : 'water';
// console.log(`I like to drink ${drink}`);

// console.log(`I like to drunk ${age >= 18 ? 'wine' : 'water'}`);

// Strict mode
