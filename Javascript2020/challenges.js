'use strict';

console.log('------ Challenges ------');

// // BMI calculation
// // const weightMark = 95;
// // const heightMark = 1.88;
// // const weightJohn = 85;
// // const heightJohn = 1.76;

// // const bmiMark = weightMark / heightMark ** 2;
// // const bmiJohn = weightJohn / (heightJohn * heightJohn);

// // console.log(bmiMark, bmiJohn);

// // --- Challenge 2
// // if (bmiMark > bmiJohn) {
// //   console.log(
// //     `Mark's BMI (${bmiMark.toFixed(
// //       2
// //     )}) is higher that John's BMI (${bmiJohn.toFixed(2)})`
// //   );
// // } else {
// //   console.log(
// //     `John's BMI (${bmiJohn.toFixed(
// //       2
// //     )}) is higher that Marks's BMI (${bmiMark.toFixed(2)})`
// //   );
// // }
// // const dolphinsScore1 = 96;
// // const dolphinsScore2 = 108;
// // const dolphinsScore3 = 89;

// // const koalaScores1 = 88;
// // const koalaScores2 = 91;
// // const koalaScores3 = 110;

// // const dolphinsScore1 = 97;
// // const dolphinsScore2 = 112;
// // const dolphinsScore3 = 101;

// // const koalaScores1 = 109;
// // const koalaScores2 = 95;
// // const koalaScores3 = 123;

// // const dolphinsScore1 = 97;
// // const dolphinsScore2 = 112;
// // const dolphinsScore3 = 80;

// // const koalaScores1 = 109;
// // const koalaScores2 = 95;
// // const koalaScores3 = 50;

// // const averageDolphinsScore =
// //   (dolphinsScore1 + dolphinsScore2 + dolphinsScore3) / 3;
// // const averageKoalaScores = (koalaScores1 + koalaScores2 + koalaScores3) / 3;

// // if (averageDolphinsScore > averageKoalaScores && averageDolphinsScore >= 100) {
// //   console.log(
// //     `Dolphins are the winners with average score ${averageDolphinsScore} against ${averageKoalaScores}`
// //   );
// // } else if (
// //   averageDolphinsScore === averageKoalaScores &&
// //   averageDolphinsScore >= 100 &&
// //   averageKoalaScores >= 100
// // ) {
// //   console.log(`It's a draw!`);
// // } else if (
// //   averageKoalaScores > averageDolphinsScore &&
// //   averageKoalaScores >= 100
// // ) {
// //   console.log(
// //     `Koalas are the winners with average score ${averageKoalaScores} against ${averageDolphinsScore}`
// //   );
// // } else {
// //   console.log('No one is a winner');
// // }

// // console.log(averageDolphinsScore, averageKoalaScores);

// // const bill = Number(prompt('Enter bill value'));
// // const tip = bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
// // console.log(
// //   `The bill was ${bill}, the tip was ${tip}, and the total value ${bill + tip}`
// // );

// // const averageScore = (score1, score2, score3) => (score1 + score2 + score3) / 3;

// // //Test 1
// // let averageScoreDolphins = averageScore(44, 23, 71);
// // let averageScoreKoalas = averageScore(65, 54, 49);

// // const checkWinner = function (avgDolphins, avgKoalas) {
// //   if (avgDolphins * 2 <= avgKoalas) {
// //     console.log(`Koalas win (${avgKoalas} vs ${avgDolphins})`);
// //   } else if (avgKoalas * 2 <= avgDolphins) {
// //     console.log(`Dolphins win (${avgDolphins} vs ${avgKoalas})`);
// //   } else {
// //     console.log(`No one wins! (${avgDolphins} vs ${avgKoalas})`);
// //   }
// // };

// // checkWinner(averageScoreDolphins, averageScoreKoalas);

// // // Test 2
// // averageScoreDolphins = averageScore(85, 54, 41);
// // averageScoreKoalas = averageScore(23, 34, 27);
// // checkWinner(averageScoreDolphins, averageScoreKoalas);

// // const calcTip = function (bill) {
// //   return bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
// // };
// // const calcTip = (bill) =>
// //   bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
// // console.log(calcTip(44));

// // const bills = [125, 555, 44];
// // const tips = [calcTip(bills[0]), calcTip(bills[1]), calcTip(bills[2])];
// // console.log(tips);
// // const total = [bills[0] + tips[0], bills[1] + tips[1], bills[2] + tips[2]];
// // console.log(total);
// /*
// const markMiller = {
//   fullName: 'Mark Miller',
//   mass: 78,
//   height: 1.69,
//   calcBMI: function () {
//     this.bmi = (this.mass / this.height ** 2).toFixed(1);
//     return this.bmi;
//   },
// };
// console.log(markMiller.calcBMI());

// const johnSmith = {
//   fullName: 'John Smith',
//   mass: 92,
//   height: 1.95,
//   calcBMI: function () {
//     this.bmi = (this.mass / this.height ** 2).toFixed(1);
//     return this.bmi;
//   },
// };
// console.log(johnSmith.calcBMI());
// console.log(
//   `${markMiller.fullName}'s BMI (${markMiller.bmi}) is ${
//     markMiller.bmi > johnSmith.bmi ? 'higher' : 'lower'
//   } than ${johnSmith.fullName} BMI (${johnSmith.bmi})!`
// );
// */
// // Coding challenge #4
// /*
// const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];
// const tips = [];
// const totals = [];
// const calcTips = (bill) =>
//   bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
// for (let i = 0; i < bills.length; i++) {
//   tips.push(calcTips(bills[i]));
//   totals.push(bills[i] + tips[i]);
// }
// console.log(bills, tips, totals);

// const calcAverage = (arr) => {
//   let sum = 0;
//   for (let i = 0; i < arr.length; i++) {
//     sum += arr[i];
//   }
//   return sum / arr.length;
// };

// console.log(calcAverage(bills));
// console.log(calcAverage(tips));
// console.log(calcAverage(totals));
// */
// /*
// const printForecast = function (arr) {
//   let str = '...';
//   for (let i = 0; i < arr.length; i++) {
//     str += ` ${arr[i]}°C in ${i + 1} days ...`;
//   }
//   return str;
// };
// const testTemps1 = [17, 21, 23];
// const testTemps2 = [12, 5, -5, 0, 4];
// console.log(printForecast(testTemps1));
// */
// console.log('------ 9 Data structures ------');
// const game = {
//   team1: 'Bayern Munich',
//   team2: 'Borrussia Dortmund',
//   players: [
//     [
//       'Neuer',
//       'Pavard',
//       'Martinez',
//       'Alaba',
//       'Davies',
//       'Kimmich',
//       'Goretzka',
//       'Coman',
//       'Muller',
//       'Gnarby',
//       'Lewandowski',
//     ],
//     [
//       'Burki',
//       'Schulz',
//       'Hummels',
//       'Akanji',
//       'Hakimi',
//       'Weigl',
//       'Witsel',
//       'Hazard',
//       'Brandt',
//       'Sancho',
//       'Gotze',
//     ],
//   ],
//   score: '4:0',
//   scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
//   date: 'Nov 9th, 2037',
//   odds: {
//     team1: 1.33,
//     x: 3.25,
//     team2: 6.5,
//   },
// };
// console.log('=== CHALLENGE 1 ===');
// // 1.
// const [players1, players2] = game.players;
// console.log(players1, players2);

// // 2.
// const [gkBayern, ...fieldPlayersBayern] = players1;
// const [gkBorrussia, ...fieldPlayersBorrussia] = players2;
// console.log(gkBayern, fieldPlayersBayern, gkBorrussia, fieldPlayersBorrussia);

// // 3.
// const allPlayers = [...players1, ...players2];
// console.log(allPlayers);

// // 4.
// const players1Final = [...players1, 'Thiago', 'Coutinho', 'Perisic'];
// console.log(players1Final);

// // 5.
// const {
//   odds: { team1, x: draw, team2 },
// } = game;
// console.log(team1, draw, team2);

// // 6.
// const printGoals = function (...players) {
//   let plSum = '';
//   for (let i = 0; i < players.length; i++) {
//     plSum += `${i + 1}.${players[i]}, `;
//   }
//   console.log(plSum, `scored ${players.length} goals in total.`);
// };
// printGoals('Davies', 'Muller', 'Lewandowski', 'Kimmich');
// printGoals(...game.scored);

// // 7.
// console.log((team2 > team1 && 'Team 2 wins') || 'Team 1 wins');

// console.log('=== CHALLENGE 2 ===');
// // 1.
// /*
// let index = 0;
// for (const scorer of Object.values(game.scored)) {
//   index += 1;
//   console.log(`Goal ${index}: ${scorer}`);
// }
// */

// // console.log(game.scored.entries());
// // console.log(Object.entries(game.scored));

// // for (const [i, player] of game.scored.entries()) {
// //   console.log(i + 1, player);
// // }

// for (const [i, player] of game.scored.entries()) {
//   console.log(`Goal ${i + 1}: ${player}`);
// }

// // 2.
// let average = 0;
// const odds = Object.values(game.odds);
// for (const odd of odds) average += odd;
// average /= odds.length;
// console.log('Average odds:', average);

// // 3.
// for (const [key, value] of Object.entries(game.odds)) {
//   // console.log(
//   //   `Odd of${game[key] ? ' victory' : ''} ${game[key] || 'draw'}: ${value}`
//   // );
//   console.log(
//     `Odd of ${key === 'x' ? 'draw' : 'victory ' + game[key]}: ${value}`
//   );
// }

// // 4.
// const scorers = {};
// for (const x of game.scored) {
//   if (Object.keys(scorers).indexOf(x) !== -1) {
//     scorers[x] += 1;
//   } else {
//     scorers[x] = 1;
//   }
// }
// console.log(scorers);
// console.log(Object.keys(scorers));

// console.log('=== CHALLENGE 3 ===');

// const gameEvents = new Map([
//   [17, '⚽️ GOAL'],
//   [36, '🔁 Substitution'],
//   [47, '⚽️ GOAL'],
//   [61, '🔁 Substitution'],
//   [64, '🔶 Yellow card'],
//   [69, '🔴 Red card'],
//   [70, '🔁 Substitution'],
//   [72, '🔁 Substitution'],
//   [76, '⚽️ GOAL'],
//   [80, '⚽️ GOAL'],
//   [92, '🔶 Yellow card'],
// ]);

// // 1.
// const setEvents = new Set([...gameEvents.values()]);
// const arrEvents = [...setEvents];
// console.log(setEvents);
// console.log(arrEvents);

// // 2.
// gameEvents.delete(64);
// console.log(gameEvents);

// // 3.
// const time = [...gameEvents.keys()].pop();
// console.log(time);
// console.log(
//   `An event happened, on average, every ${time / gameEvents.size} minutes`
// );

// // 4.
// for (const [minute, event] of gameEvents) {
//   console.log(
//     `${minute <= 45 ? '[FIRST' : '[SECOND'} HALF] ${minute}: ${event}`
//   );
// }
// /*
// console.log('=== CHALLENGE 4 ===');

// document.body.append(document.createElement('textarea'));
// document.body.append(document.createElement('button'));

// const txtArea = document.querySelector('textarea');
// const btnSubmit = document.querySelector('button');

// btnSubmit.addEventListener('click', function () {
//   const arrVar = [];
//   const rows = txtArea.value.toLowerCase().split('\n');
//   for (const [i, row] of rows.entries()) {
//     const [first, second] = row.trim().split('_');
//     const output = `${first}${second.replace(
//       second[0],
//       second[0].toUpperCase()
//     )}`;
//     console.log(output.padEnd(20) + '🃟'.repeat(i + 1));
//   }
// });
// */
// /*
// underscore_case
//  first_name
//  Some_Variable
//   calculate_AGE
// delayed_departure
// */
/*
console.log('=== CHALLENGE 1 Functions ===');
const poll = {
  question: 'What is your favourite programing language?',
  options: ['0: Javascript', '1: Python', '2: Rust', '3: C++'],
  answers: new Array(4).fill(0),
  registerNewAnswer() {
    const input = Number(
      prompt(
        `${this.question}\n${[this.options.join('\n')]}\n(Write option number)`
      )
    );
    input >= 0 && input <= 3 && this.answers[input]++;
    this.displayResults();
    this.displayResults('string');
  },
  displayResults(type = 'array') {
    if (type === 'array') {
      console.log(this.answers);
    } else if (type === 'string') {
      console.log(`Poll results are ${this.answers.join(', ')}`);
    }
  },
};

document
  .querySelector('.poll')
  .addEventListener('click', poll.registerNewAnswer.bind(poll));

// const answers = [5, 2, 3];
poll.displayResults.call({ answers: [5, 2, 3, 4, 5, 6] }, 'string');

//
(function () {
  const header = document.querySelector('h1');
  header.style.color = 'red';
  document.body.addEventListener('click', () => {
    header.style.color = 'blue';
  });
})();
*/
// const dogsJulia = [3, 5, 2, 12, 7];
// const dogsKate = [4, 1, 15, 8, 3];
const dogsJulia = [9, 16, 6, 8, 3];
const dogsKate = [10, 5, 6, 1, 4];

const checkDogs = function (arr1, arr2) {
  const copyArr1 = arr1.slice(1, arr1.length - 2);
  console.log(copyArr1);
  const array = [...copyArr1, ...arr2];
  console.log(array);
  array.forEach(function (age, i) {
    console.log(
      `${
        age >= 3
          ? `Dog number ${i + 1} is an adult, and is ${age} years old`
          : `Dog number ${i + 1} is still a puppy`
      }`
    );
  });
};
// checkDogs(dogsJulia, dogsKate);
/*
const calcAverageHumanAge = function (ages) {
  const humanAge = ages.map((age) => (age <= 2 ? age * 2 : 16 + age * 4));
  const filterAdult = humanAge.filter((age) => age >= 18);
  const averageAgeAdults =
    filterAdult.reduce((prev, curr) => prev + curr) / filterAdult.length;
  console.log(humanAge);
  console.log(filterAdult);
  console.log(averageAgeAdults);
};
calcAverageHumanAge([5, 2, 4, 1, 15, 8, 3]);
calcAverageHumanAge([16, 6, 10, 5, 6, 1, 4]);
*/

const calcAverageHumanAgeChain = function (ages) {
  const averageAgeAdults = ages
    .map((age) => (age <= 2 ? age * 2 : 16 + age * 4))
    .filter((age) => age >= 18)
    .reduce((acc, age, i, arr) => acc + age / arr.length, 0);
  // console.log(averageAgeAdults);
};
calcAverageHumanAgeChain([5, 2, 4, 1, 15, 8, 3]);
calcAverageHumanAgeChain([16, 6, 10, 5, 6, 1, 4]);

// Challenge #4

const dogs = [
  { weight: 22, curFood: 250, owners: ['Alice', 'Bob'] },
  { weight: 8, curFood: 200, owners: ['Matilda'] },
  { weight: 13, curFood: 275, owners: ['Sarah', 'John'] },
  { weight: 32, curFood: 340, owners: ['Michael'] },
];

const eatingNormal = function (dog) {
  const curFood = dog.curFood;
  const minFood = dog.recommendedFood * 0.9;
  const maxFood = dog.recommendedFood * 1.1;
  if (curFood > minFood && curFood < maxFood) {
    return true;
  } else if (curFood < minFood) {
    return 'too little';
  } else if (curFood > maxFood) {
    return 'too much';
  } else if (curFood === dog.recommendedFood) {
    return 'exactly';
  }
};

const eatingOK = (dog) => {
  const curFood = dog.curFood;
  const minFood = dog.recommendedFood * 0.9;
  const maxFood = dog.recommendedFood * 1.1;
  return curFood > minFood && curFood < maxFood;
};

// 1
dogs.forEach(
  (dog) => (dog.recommendedFood = Math.trunc(dog.weight ** 0.75 * 28))
);

// 2
const sarahsDog = dogs.find((dog) => dog.owners.includes('Sarah'));
console.log(
  eatingNormal(sarahsDog) === 'okay'
    ? `Sarah's dog eating okay daily dose.`
    : `Sarah's dog eating ${eatingNormal(sarahsDog)}!`
);

// 3
// const downersEatTooLittle = dogs.map((dog) => {
//   if (eatingNormal(dog) === 'too little') {
//     return dog.owners;
//   }
// });
// console.log(downersEatTooLittle.flat().filter((el) => el != null));

// const ownersEatTooMuch = dogs.map((dog) => {
//   if (!dog.owners) continue;
//   if (eatingNormal(dog) === 'too much') {
//     return dog.owners;
//   }
// });
const ownersEatTooLittle = [];
const ownersEatTooMuch = [];
for (const dog of dogs) {
  if (eatingNormal(dog) === 'too little')
    ownersEatTooLittle.push(...dog.owners);
  if (eatingNormal(dog) === 'too much') ownersEatTooMuch.push(...dog.owners);
}
console.log(ownersEatTooLittle, ownersEatTooMuch);

// 4
console.log(`${ownersEatTooLittle.join(' and ')}'s dogs eat too little!`);
console.log(`${ownersEatTooMuch.join(' and ')}'s dogs eat too much!`);

// 5
console.log(dogs.some((dog) => eatingNormal(dog) === 'exactly'));

// 6
console.log(dogs.some((dog) => eatingNormal(dog) === 'okay'));

// 7
// const eatingOkDogs = [];
// dogs.some((dog) => {
//   if (eatingNormal(dog) === 'okay') eatingOkDogs.push(dog);
// });
// console.log(eatingOkDogs);
console.log(dogs.filter(eatingOK));

// 8
const sortedDogs = dogs
  .slice()
  .sort((a, b) => a.recommendedFood - b.recommendedFood);
console.log(sortedDogs);

// OBJECTS
const Car = function (make, speed) {
  this.make = make;
  this.speed = speed;
};
Car.prototype.accelerate = function () {
  this.speed += 10;
  console.log(`${this.make} is going at ${this.speed} km/h`);
};
Car.prototype.brake = function () {
  this.speed -= 5;
  console.log(`${this.make} is going at ${this.speed} km/h`);
};

const bmw = new Car('BMW', 120);
bmw.accelerate();
bmw.brake();

const mercedes = new Car('Mercedes', 95);
mercedes.brake();
mercedes.accelerate();

console.log(bmw);
console.log(mercedes);

// OBJECTS ES6
class CarES6 {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }
  accelerate() {
    this.speed += 10;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }
  brake() {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }
  get speedUS() {
    return this.speed / 1.6;
  }
  set speedUS(speed) {
    this.speed = speed * 1.6;
  }
}

const carES6Mers = new CarES6('Mersedes', 120);
carES6Mers.brake();
carES6Mers.accelerate();
carES6Mers.speedUS = 65;
console.log(carES6Mers.speedUS);
console.log(carES6Mers.speed);

// Challenge #3
const EV = function (make, speed, charge) {
  Car.call(this, make, speed);
  this.charge = charge;
};
EV.prototype = Object.create(Car.prototype);

EV.prototype.chargeBattery = function (chargeTo) {
  this.charge = chargeTo;
  console.log(`${this.make} is charged to ${this.charge}%`);
};
EV.prototype.accelerate = function () {
  this.speed += 20;
  this.charge--;
  console.log(
    `${this.make} is going at ${this.speed} km/h, with a charge of ${this.charge}%`
  );
};

const tesla = new EV('Tesla', 120, 23);
tesla.accelerate();
tesla.accelerate();
tesla.chargeBattery(90);
tesla.accelerate();
tesla.brake();

console.log(tesla);

// #4

class CarCl {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }
  accelerate() {
    this.speed += 10;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }
  brake() {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed} km/h`);
    return this;
  }
  get speedUS() {
    return this.speed / 1.6;
  }
  set speedUS(speed) {
    this.speed = speed * 1.6;
  }
}

class EVCl extends CarCl {
  #charge;

  constructor(make, speed, charge) {
    super(make, speed);
    this.#charge = charge;
  }

  chargeBattery(chargeTo) {
    this.#charge = chargeTo;
    console.log(`${this.make} is charged to ${this.#charge}%`);
    return this;
  }

  accelerate() {
    this.speed += 20;
    this.#charge--;
    console.log(
      `${this.make} is going at ${this.speed} km/h, with a charge of ${
        this.#charge
      }%`
    );
    return this;
  }
}

const rivian = new EVCl('Rivian', 120, 23);
rivian.accelerate();
rivian.brake();
rivian.chargeBattery(85);
rivian.accelerate();
rivian.brake().accelerate().brake();

//////
console.log('CHALLENGE 16 PROMISES');

const whereAmI = function (lat, lon) {
  fetch(
    `https://us1.locationiq.com/v1/reverse.php?key=pk.85ba6bc16f21f87c70351cbae9bd3105	&lat=${lat}&lon=${lon}&format=json`
  )
    .then((response) => {
      if (!response.ok)
        throw new Error(`Problem with geolocation ${response.status}`);
      return response.json();
    })
    .then((data) => {
      printCity(data.address);
      getCountryData(data.address.country);
    })

    .catch((err) => console.error(`ERROR ${err.message}`));
};

const printCity = function (address) {
  console.log(`You are in ${address.city}, ${address.country}`);
};

// whereAmI(52.508, 13.381);
// whereAmI(19.037, 72.873);
whereAmI(-33.933, 18.474);
