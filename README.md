# Jonas Schmedtmann

#### Web Developer, Designer, and Teacher

![Jonas Schmedtmann](https://img-a.udemycdn.com/user/200_H/7799204_2091_5.jpg)

## About me

Hi, I'm Jonas! I have been identified as one of Udemy's Top Instructors and all my premium courses have recently earned the best-selling status for outstanding performance and student satisfaction.

I'm a full-stack web developer and designer with a passion for building beautiful things from scratch. I've been building websites and apps since 2007 and also have a Master's degree in Engineering.

It was in college where I first discovered my passion for teaching and helping others by sharing all I knew. And that passion brought me to Udemy in 2015, where my students love the fact that I take the time to explain important concepts in a way that everyone can easily understand.

**_Do you want to learn how to build awesome websites with advanced HTML and CSS?_**

**_Looking for a complete JavaScript course that takes you from beginner to advanced developer?_**

**_Or maybe you want to build modern and fast back-end applications with Node.js?_**

Then don't waste your time with random tutorials or incomplete videos. All my courses are easy-to-follow, all-in-one packages that will take your skills to the next level.

_These courses are exactly the courses I wish I had when I was first getting into web development!_

So see for yourself, enroll in one of my courses (or all of them :D) and join my 500,000+ happy students today.
